#include <utility>

#ifndef __PROGTEST__
#include <cassert>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>
using namespace std;
#endif /* __PROGTEST__ */

class Component{
public:

    explicit Component(string componentName):_componentName(std::move(componentName)){

    }
    Component (const Component &comp){
        _componentName= comp._componentName;
        _partSize= comp._partSize;
        _partName= comp._partName;
        _size= comp._size;
        _type= comp._type;
        _cores= comp._cores;
        _freq= comp._freq;
    }
    template <typename _T>
    Component& operator = (const _T & comp){
        _componentName= comp._componentName;
        _partSize= comp._partSize;
        _partName= comp._partName;
        _size= comp._size;
        _type= comp._type;
        _cores= comp._cores;
        _freq= comp._freq;
        return *this;
    }


    void setComponentName(const string &componentName) {
        _componentName = componentName;
    }

    const string &getComponentName() const {
        return _componentName;
    }
    void adPart(int size,const string & partitionName){


        _partName.push_back(partitionName);
        _partSize.push_back(size);
    }

    virtual stringstream getInfo()const{
        stringstream str;
        return str;
    }
    virtual ~Component(){

    }
protected:
    string _componentName="";
    vector<int > _partSize;
    vector<string > _partName;
    int _size=0;
    int _type=0;
    int _cores=0, _freq=0;

};
class CCPU : public Component{
public:
    CCPU(int cores,int freq):Component("CPU"){
        _cores=cores;
        _freq=freq;
    }

    stringstream getInfo()const override{
        string info;
        info=_componentName+", "+to_string(_cores)+" cores @ "+to_string(_freq)+"MHz"+"\n";
        stringstream stream;
        stream.str(info);
        return stream;
    }
private:
    int _cores1=0, _freq1=0;

};
class CMemory : public Component {
public:
    CMemory(int size):Component("Memory"){
        _size=size;
    }
    stringstream getInfo()const override {
        string info;
        info=_componentName+", "+to_string(_size)+" MiB"+"\n";
        stringstream stream;
        stream.str(info);
        return stream;
    }
private:
    int _size1=0;

};

class CDisk : public Component{
public:


    const static int SSD=0;
    const static int MAGNETIC=1;
    CDisk(int type,int size):Component(""){
        _type=type;
        _size=size;
        if(type==1){
            this->setComponentName("HDD");
        }else if(type==0){
            this->setComponentName("SSD");
        }
    }
    CDisk(const CDisk & disk):Component(""){
        _partSize=disk._partSize;
        _partName=disk._partName;
        _size=disk._size;
        _type=disk._type;
        this->setComponentName(disk.getComponentName());

    }
    CDisk & AddPartition(int size,const string & partitionName){
        adPart(size,partitionName);
        return *this;
    }

    stringstream getInfo()const override {
        stringstream stream;
        stream<<_componentName<<", "<<_size<<" GiB"<<endl;

        for(unsigned int i=0;i<_partName.size();i++){
            if(i==(_partName.size()-1)){
                stream<<"\\-";
            }else{
                stream<<"+-";
            }

            stream<<"["<<i<<"]: "<<_partSize[i]<<" GiB, "<<_partName[i]<<endl;
        }

        return stream;
    }
private:
    int _type1=0;
    vector<int > _partSize1;
    vector<string > _partName1;

};

class CComputer{
public:
    CComputer (string  computerName): _computerName(std::move(computerName)){

    }
    CComputer (const CComputer & computer ){
        _computerName=computer._computerName;
        _addresses=computer._addresses;

        for(unsigned int i=0;i<computer._components.size();i++){
            Component * com=new Component(*computer._components[i]);
            _components.push_back(com);
        }
    }

    CComputer & operator =(const CComputer & computer){


        _computerName=computer._computerName;
        _addresses=computer._addresses;
        for(auto & component:_components){
            delete component;
        }
        for(unsigned int i=0;i<computer._components.size();i++){
            Component * com=new Component(*computer._components[i]);
            _components.push_back(com);
        }
        return *this;

    }
    CComputer& AddAddress(const string& addressName){
        _addresses.push_back(addressName);
        return *this;
    }
    template <typename _component>
    CComputer& AddComponent( _component  x){
    Component * comp=new Component(x);

    _components.push_back(comp);
    return *this;
    }
    const string &getComputerName() const {
        return _computerName;
    }

    stringstream getStream()const{
        stringstream ss;
        ss<<"Host: "<<_computerName<<"\n";
        for(unsigned int i=0;i<_addresses.size();i++){
            if(i==(_addresses.size()-1)&&_components.empty()){
                ss<< "\\-";
            }else {
                ss<< "+-";
            }
            ss<<_addresses[i]<<endl;
        }
        stringstream componentStr;
        string line;

        for(unsigned int i=0;i<_components.size();i++){

            if(_components[i]->getComponentName()=="CPU"){

                auto cpu= static_cast<CCPU*>(_components[i]);
                componentStr=cpu->CCPU::getInfo();
            }else if(_components[i]->getComponentName()=="Memory"){
                auto memory= static_cast<CMemory*>(_components[i]);
                componentStr=memory->CMemory::getInfo();
            }else{
                auto disk= static_cast<CDisk*>(_components[i]);
                componentStr=disk->CDisk::getInfo();
            }

            if(i==(_components.size()-1)){
                ss<< "\\-";
            }else {
                ss<< "+-";
            }
            getline(componentStr,line);
            ss<<line<<endl;
            while(!componentStr.eof()){
                getline(componentStr,line);
                if(componentStr.eof()){
                    break;
                }
                if(i==(_components.size()-1)){
                    ss<<"  ";
                }else {
                    ss<<"| ";
                }
                ss<<line<<endl;
            }


        }
        return ss;
    }

    friend ostream & operator <<(ostream & ostr,const CComputer &computer){
        stringstream comp=computer.getStream();
        ostr<< comp.rdbuf();
        return ostr;
    }

    vector<Component*> _components;

    ~CComputer(){
        if(_components.empty()){
            return;
        }
        for(auto & component:_components){
            delete component;
        }
    }
protected:
    string _computerName;
    vector<string> _addresses;

};
class CNetwork {
public:
    explicit CNetwork (string  networkName): _networkName(std::move(networkName)){

    }
    CNetwork & operator = (const CNetwork & network){
        _networkName=network._networkName;
        _computers.clear();
        _computers=network._computers;
        return *this;
    }
    CNetwork & AddComputer( const CComputer & computer){

        _computers.push_back(computer);
        return *this;
    }
    CComputer* FindComputer(const string &name){
        for(unsigned int i=0;i<_computers.size();i++){
            if(_computers[i].getComputerName()==name){
                return &_computers[i];
            }
        }
        return &_computers[_computers.size()];
    }
    friend ostream & operator <<(ostream & ostr,const CNetwork &network){
        ostr<<"Network: "<<network._networkName<<endl;
        stringstream pcStr;
        string line;
        for (unsigned int i=0;i<network._computers.size();i++){
            pcStr=network._computers[i].getStream();
            if(i==(network._computers.size()-1)){
                ostr<<"\\-";
            }else {
                ostr << "+-";
            }
            getline(pcStr,line);
            ostr<<line<<endl;
            while(!pcStr.eof()){
                getline(pcStr,line);
                if(pcStr.eof()){
                    break;
                }
                if(i==(network._computers.size()-1)){
                    ostr<<"  ";
                }else {
                    ostr<<"| ";
                }
                ostr<<line<<endl;
            }
        }

        return ostr;
    }

private:
    string _networkName;
    vector<CComputer> _computers;


};
#ifndef __PROGTEST__
template<typename _T>
string toString ( const _T & x )
{
    ostringstream oss;
    cout<<x<<endl;
    oss << x;
    return oss . str ();
}


int main ( void )
{
    cout << sizeof(CCPU)<<"------"<< sizeof(CDisk)<<endl;
    CNetwork n ( "FIT network" );
    CNetwork n2 ("cdfvd");
    n . AddComputer (
            CComputer ( "progtest.fit.cvut.cz" )
            .
                    AddAddress ( "147.32.232.142" ) .
                    AddComponent ( CCPU ( 8, 2400 ) ) .
                    AddComponent ( CCPU ( 8, 1200 ) ) .
                    AddComponent ( CDisk ( CDisk::MAGNETIC, 1500 ) .
                    AddPartition ( 50, "/" ) .
                    AddPartition ( 5, "/boot" ).
                    AddPartition ( 1000, "/var" ) ) .
                    AddComponent ( CDisk ( CDisk::SSD, 60 ) .
                    AddPartition ( 60, "/data" )  ) .
                    AddComponent ( CMemory ( 2000 ) ).
                    AddComponent ( CMemory ( 2000 ) )
                    ) .
            AddComputer (
            CComputer ( "edux.fit.cvut.cz" )
            .
                    AddAddress ( "147.32.232.158" ) .
                    AddComponent ( CCPU ( 4, 1600 ) ) .
                    AddComponent ( CMemory ( 4000 ) ).
                    AddComponent ( CDisk ( CDisk::MAGNETIC, 2000 ) .
                    AddPartition ( 100, "/" )   .
                    AddPartition ( 1900, "/data" ) )
                    ) .
            AddComputer (
            CComputer ( "imap.fit.cvut.cz" ) .
                    AddAddress ( "147.32.232.238" ) .
                    AddComponent ( CCPU ( 4, 2500 ) ) .
                    AddAddress ( "2001:718:2:2901::238" ) .
                    AddComponent ( CMemory ( 8000 ) ) );

    n2=n;


    assert ( toString ( n ) ==
             "Network: FIT network\n"
             "+-Host: progtest.fit.cvut.cz\n"
             "| +-147.32.232.142\n"
             "| +-CPU, 8 cores @ 2400MHz\n"
             "| +-CPU, 8 cores @ 1200MHz\n"
             "| +-HDD, 1500 GiB\n"
             "| | +-[0]: 50 GiB, /\n"
             "| | +-[1]: 5 GiB, /boot\n"
             "| | \\-[2]: 1000 GiB, /var\n"
             "| +-SSD, 60 GiB\n"
             "| | \\-[0]: 60 GiB, /data\n"
             "| +-Memory, 2000 MiB\n"
             "| \\-Memory, 2000 MiB\n"
             "+-Host: edux.fit.cvut.cz\n"
             "| +-147.32.232.158\n"
             "| +-CPU, 4 cores @ 1600MHz\n"
             "| +-Memory, 4000 MiB\n"
             "| \\-HDD, 2000 GiB\n"
             "|   +-[0]: 100 GiB, /\n"
             "|   \\-[1]: 1900 GiB, /data\n"
             "\\-Host: imap.fit.cvut.cz\n"
             "  +-147.32.232.238\n"
             "  +-2001:718:2:2901::238\n"
             "  +-CPU, 4 cores @ 2500MHz\n"
             "  \\-Memory, 8000 MiB\n" );
    CNetwork x = n;
    auto c = x . FindComputer ( "imap.fit.cvut.cz" );




    assert ( toString ( *c ) ==
             "Host: imap.fit.cvut.cz\n"
             "+-147.32.232.238\n"
             "+-2001:718:2:2901::238\n"
             "+-CPU, 4 cores @ 2500MHz\n"
             "\\-Memory, 8000 MiB\n" );
    auto ca= *c;
    c -> AddComponent ( CDisk ( CDisk::MAGNETIC, 1000 ) .
            AddPartition ( 100, "system" ) .
            AddPartition ( 200, "WWW" ) .
            AddPartition ( 700, "mail" ) );
    assert ( toString ( x ) ==
             "Network: FIT network\n"
             "+-Host: progtest.fit.cvut.cz\n"
             "| +-147.32.232.142\n"
             "| +-CPU, 8 cores @ 2400MHz\n"
             "| +-CPU, 8 cores @ 1200MHz\n"
             "| +-HDD, 1500 GiB\n"
             "| | +-[0]: 50 GiB, /\n"
             "| | +-[1]: 5 GiB, /boot\n"
             "| | \\-[2]: 1000 GiB, /var\n"
             "| +-SSD, 60 GiB\n"
             "| | \\-[0]: 60 GiB, /data\n"
             "| +-Memory, 2000 MiB\n"
             "| \\-Memory, 2000 MiB\n"
             "+-Host: edux.fit.cvut.cz\n"
             "| +-147.32.232.158\n"
             "| +-CPU, 4 cores @ 1600MHz\n"
             "| +-Memory, 4000 MiB\n"
             "| \\-HDD, 2000 GiB\n"
             "|   +-[0]: 100 GiB, /\n"
             "|   \\-[1]: 1900 GiB, /data\n"
             "\\-Host: imap.fit.cvut.cz\n"
             "  +-147.32.232.238\n"
             "  +-2001:718:2:2901::238\n"
             "  +-CPU, 4 cores @ 2500MHz\n"
             "  +-Memory, 8000 MiB\n"
             "  \\-HDD, 1000 GiB\n"
             "    +-[0]: 100 GiB, system\n"
             "    +-[1]: 200 GiB, WWW\n"
             "    \\-[2]: 700 GiB, mail\n" );
    assert ( toString ( n ) ==
             "Network: FIT network\n"
             "+-Host: progtest.fit.cvut.cz\n"
             "| +-147.32.232.142\n"
             "| +-CPU, 8 cores @ 2400MHz\n"
             "| +-CPU, 8 cores @ 1200MHz\n"
             "| +-HDD, 1500 GiB\n"
             "| | +-[0]: 50 GiB, /\n"
             "| | +-[1]: 5 GiB, /boot\n"
             "| | \\-[2]: 1000 GiB, /var\n"
             "| +-SSD, 60 GiB\n"
             "| | \\-[0]: 60 GiB, /data\n"
             "| +-Memory, 2000 MiB\n"
             "| \\-Memory, 2000 MiB\n"
             "+-Host: edux.fit.cvut.cz\n"
             "| +-147.32.232.158\n"
             "| +-CPU, 4 cores @ 1600MHz\n"
             "| +-Memory, 4000 MiB\n"
             "| \\-HDD, 2000 GiB\n"
             "|   +-[0]: 100 GiB, /\n"
             "|   \\-[1]: 1900 GiB, /data\n"
             "\\-Host: imap.fit.cvut.cz\n"
             "  +-147.32.232.238\n"
             "  +-2001:718:2:2901::238\n"
             "  +-CPU, 4 cores @ 2500MHz\n"
             "  \\-Memory, 8000 MiB\n" );
    return 0;
}
#endif /* __PROGTEST__ */
